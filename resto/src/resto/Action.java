package resto;

public enum Action {
	attendre,
	attribuerPlace,
	servir,
	encaisser,
	demanderPlace,
	commander,
	payer,
	quitter;
}
