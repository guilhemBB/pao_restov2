package resto;

import java.util.ArrayList;

public class Environnement {
	public int nbPlacesDispo;
	public ArrayList<Integer> dispoPlats;
	public ArrayList<Serveur> serveurs;
	public ArrayList<Client> clients;
	public int nbPlatsServisJour=0;
	
	public Environnement(int nbPlaces,int nbPlats, int nbClients,int nbServeurs) {
		this.nbPlacesDispo = nbPlaces;
		this.dispoPlats = new ArrayList<>(nbPlats);
		this.serveurs = new ArrayList<>();
		this.clients = new ArrayList<>();
		
		for(int i=0;i<nbClients;i++) {
			this.clients.add(new Client("Client "+(i+1),this));
		}
		for(int i=0;i<nbServeurs;i++) {
			this.serveurs.add(new Serveur("Serveur "+(i+1),this));
		}
		for(int i=0;i< nbPlats;i++)//M
		{
			this.dispoPlats.add(i);
		}
		
	}
	
	public void run() {
		for (Client c : this.clients) {
		    c.doAction();
		}
		for (Serveur s : this.serveurs) {
		    s.doAction();
		}
	}
	public ArrayList<Client> getClients() {
		return this.clients;
	}
	public ArrayList<Serveur> getServeurs() {
		return this.serveurs;
	}
	public int getNbPlacesDispo() {
		return nbPlacesDispo;
	}

	
	
	public int getNbPlatsServisJour() {
		return nbPlatsServisJour;
	}

	public void setNbPlatsServisJour(int nbPlatsServisJour) {
		this.nbPlatsServisJour = nbPlatsServisJour;
	}

	public void setNbPlacesDispo(int nbPlacesDispo) {
		this.nbPlacesDispo = nbPlacesDispo;
	}

	public ArrayList<Integer> getDispoPlats() {
		return dispoPlats;
	}

	
}