package resto;

import java.util.ArrayList;

public class Serveur extends Agent {
	private Client clientActuel=null;
	private ArrayList<Client> clients = null;
	private Environnement e =null;

	public Serveur(String n,Environnement e) {
		this.nom = n;
		this.etat = Action.attendre;
		this.clients= e.getClients();
		this.e=e;
	}

	@Override
	public Boolean doAction() {
		switch(etat) {
		case attendre:
			return this.attendre();
		case servir:
			return this.servir(clientActuel,clientActuel.getPlatPref()); //M
		case encaisser:
			return this.encaisser(clientActuel);
		case attribuerPlace:
			return this.attribuerPlace(clientActuel);
		default:
			return false;

		}
	}

	public Boolean attendre() {
		if(e.nbPlatsServisJour<e.clients.size()) {
			System.out.println(this.nom+" attend");
			for(Client c: this.clients) {
				if(c.etat == Action.demanderPlace) {
					this.clientActuel=c;
					this.etat = Action.attribuerPlace;
					break;
				}
	
			}
			return this.doAction();
			}
		else {
			this.etat = Action.quitter;
			return false;
		}
	}
	
	public Boolean attribuerPlace(Client C) {
		if(e.getNbPlacesDispo()>0){
		System.out.println(this.nom+" attribue une place au "+this.clientActuel.nom);
		this.etat = Action.servir;
		e.setNbPlacesDispo(e.getNbPlacesDispo()-1); //
		//this.clientActuel.doAction();
		return this.doAction();
		}
		else {
			System.out.println(this.nom+" ne peut pas attribuer de place au "+this.clientActuel.nom+" il n'y a plus de place disponible");
			this.etat = Action.attendre;
			return false;
		}
	}
	public Boolean servir(Client c, int plat) { //
		e.dispoPlats.set(plat,e.dispoPlats.get(plat));
		System.out.println(this.nom+" sert un plat au "+this.clientActuel.nom);
		e.setNbPlatsServisJour(e.getNbPlatsServisJour()+1);
		this.etat = Action.encaisser;
		return this.doAction();
	}
	public Boolean encaisser(Client c) {
		System.out.println("Encaisser");
		e.setNbPlacesDispo(e.getNbPlacesDispo()+1); //
		if(e.getNbPlatsServisJour()<e.clients.size()) {
			this.clientActuel.etat = Action.attendre;
			this.clientActuel=null;
			this.etat= Action.attendre;

			}
		else {
			
			this.etat= Action.quitter;

		}
		return false;
	}


}