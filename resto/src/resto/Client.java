package resto;

import java.util.ArrayList;
import java.util.Random;//M

public class Client extends Agent {
	private Serveur serveurActuel=null;
	private ArrayList<Serveur> serveurs=null;
	private ArrayList<Integer> platsDesire=new ArrayList<>(4);//M
	boolean dejaServis;

	public Client(String n,Environnement e) {
		Random random = new Random();//M
		random.setSeed(200);//M
		this.nom = n;
		this.etat = Action.attendre;
		this.serveurs=e.getServeurs();
		for(int i=0;i<4;i++) //M
		{
			this.platsDesire.add(random.nextInt(10));
		}
		dejaServis = false;
	}
	int getPlatPref() //M
	{
		int indice=0,max=0;
		for(int i=0;i<4;i++)
		{
			if(this.platsDesire.get(i)>max)
			{
				max = this.platsDesire.get(i);
				indice = i;
			}
		}
				
		return this.platsDesire.get(indice);
	}
	

	@Override
	public Boolean doAction() {
		switch(etat) {
		case attendre:
			return this.attendre();
		case demanderPlace:
			return this.demanderPlace();
		case commander:
			return this.commander(serveurActuel);
		case payer:
			return this.payer(serveurActuel);
		default:
			return false;

		}
	}

	public Boolean attendre() {
		if(this.dejaServis==false) {
		System.out.println(this.nom +" attend");
		for(Serveur s: this.serveurs) {
			if(s.etat == Action.attendre && (Math.random() * 10)<(10/3) ) {
				this.serveurActuel=s;
				this.etat = Action.demanderPlace;
				return this.doAction();
			}
		}
		if(Math.random()<0.9f){
			this.etat = Action.attendre;
		}else {
			this.etat = Action.quitter;
		}

		return this.doAction();
		}
		else return false;
	}

	public Boolean demanderPlace() {
		System.out.println(this.nom+" demande une place au "+this.serveurActuel.nom);
		this.serveurActuel.doAction();
		this.etat = Action.commander;
		return this.doAction();
	}
	public Boolean commander(Serveur s) {
		System.out.println("Commander");
		this.etat = Action.payer;
		return this.doAction();
	}
	public Boolean payer(Serveur s) {
		System.out.println("Payer");
		this.etat = Action.quitter;
		this.serveurActuel=null;
		return this.doAction();
	}


}